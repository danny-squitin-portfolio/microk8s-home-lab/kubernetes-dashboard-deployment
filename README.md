# Kubernetes Dashboard Deployment

This repository supplements the Microk8s Kubernetes Dashboard add-on with manifests that enable ingress and a cluster-admin ServiceAccount/ClusterRoleBindings.

## Repository Structure

```sh
├── bases
│   ├── kubernetes-dashboard
│   │   ├── kubernetes-dashboard-admin.crb.yaml
│   │   ├── kubernetes-dashboard-admin.sa.yaml
│   │   └── kustomization.yaml
│   └── kustomization.yaml
└── environments
    └── prod
        ├── kubernetes-dashboard.cert.yaml
        ├── kubernetes-dashboard.ing.yaml
        ├── kustomization.yaml
        └── secrets
            ├── kubernetes-dashboard-admin.sealedsecret.yaml
            └── kustomization.yaml
```

## Accessing the Dashboard

To access the Kubernetes Dashboard:

https://kubernetes-dashboard.cloudlan/
